/* global describe, it */
'use strict'

const Promise = require('bluebird')
const uuid = require('uuid/v4')
const chai = require('chai')
chai.use(require('dirty-chai'))
const expect = chai.expect

const redisConnect = require('../../../main')

describe('integration tests of redis', function () {
  describe('redis-connect', function () {
    it('should work', async function () {
      if (process.env.CI) { // don't run this in CI pipeline
        console.log('skipping because in CI pipeline')
        return
      }

      this.timeout(10000)

      const redisClient = await redisConnect({
        port: redisConnect.defaultPort
      })
      expect(redisClient).to.be.ok()

      const hash = uuid()
      const hset = Promise.promisify(redisClient.hset, { context: redisClient })
      const hget = Promise.promisify(redisClient.hget, { context: redisClient })
      const hexists = Promise.promisify(redisClient.hexists, { context: redisClient })

      const key = 'foo'
      const val = 'bar'
      expect(await hset(hash, key, val)).to.equal(1)
      expect(await hexists(hash, key)).to.equal(1)
      expect(await hget(hash, key)).to.equal('bar')

      const hdel = Promise.promisify(redisClient.hdel, { context: redisClient })

      expect(await hdel(hash, key)).to.equal(1)
      expect(await hexists(hash, key)).to.equal(0)
    })
  })
})
