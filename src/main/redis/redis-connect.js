'use strict'

const fs = require('fs')
const Redis = require('redis')

const startRedis = require('./start-redis')

const defaultPort = parseInt(fs.readFileSync(`${__dirname}/default-redis-test-port`))
const defaultContainerName = fs.readFileSync(`${__dirname}/default-redis-test-container`).toString('utf8').trim()

let client

async function redisConnect ({ // redis.creatClient opts
  port = defaultPort
} = {}, { // redisConnect opts
  waitUntilReady = true
} = {}) {
  if (client) return client

  arguments[0] = arguments[0] || {}
  arguments[0].port = arguments[0].port || defaultPort

  let start = Date.now()
  if (!process.env.CI) {
    await startRedis({ scriptArgs: arguments })
    console.log(`started redis container in ${Date.now() - start} ms`)
  } else {
    console.log('skipped launching container')
  }

  start = Date.now()
  client = Redis.createClient(arguments[0])

  return waitUntilReady
    ? new Promise((resolve, reject) => {
      client.on('error', reject)
      client.on('ready', () => {
        console.log(`connected to redis in ${Date.now() - start} ms`)
        resolve(client)
      })
    })
    : client
}

redisConnect.defaultPort = defaultPort
redisConnect.defaultContainerName = defaultContainerName

module.exports = redisConnect
