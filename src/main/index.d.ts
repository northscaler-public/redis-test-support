import * as Redis from "redis"

declare function redisConnect(): Promise<Redis.RedisClient>

export = redisConnect
