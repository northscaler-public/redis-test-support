# `redis-test-support`

Handy-dandy Redis integration testing utility that starts a local Docker container running Redis if you're not running in a CI/CD pipeline.
This allows you to run integration tests locally in a manner similar to how they'd be run in the CI/CD pipeline.
This module does nothing when running in a CI build pipeline, because Redis should be configured as part of the build via something like [`.gitlab-ci.yml`'s `services`](https://docs.gitlab.com/ee/ci/yaml/#services) element.

This package is intended to be installed in your project in `devDependencies`.

Your application must install its desired version of [`redis`](https://www.npmjs.com/package/redis).

> NOTE: requires a Unix-y shell (`/usr/bin/env sh`) to be available.
>This is not designed to run on Windows; PRs/MRs welcome.

See [src/test/integration/redis/redis.spec.js] for usage, but it's basically
```javascript
const redisConnect = require('redis-test-support')

const redisClient = await redisConnect()

// now you can use redis client
```

## Configuration

The default configuration is pretty conventional, with the sole exception of the default port that Redis will listen on for clients.
Instead of `6379`, which might already be in use on developers' machines when they run integration tests, the default configuration uses `46379`.
It is a `TODO` to search for an available port.

>NOTE: This module detects when it's running in a CI/CD pipeline by seeing if the environment variable `CI` is of nonzero length.

### Environment variables

The following environment variables can be set to configure it:
* REDIS_TEST_SUPPORT_TAG: The tag of the [`redis` Docker image](https://hub.docker.com/_/redis)  or custom image to use, default "latest"
* REDIS_TEST_SUPPORT_PORT: visible client port on `localhost` to map to container port, default is content of `redis/default-redis-test-port`
* REDIS_TEST_SUPPORT_CONTAINER: name of container, default is content of file `redis/default-redis-test-container`
* REDIS_TEST_SUPPORT_CONTAINER_PORT: redis client port in container, default `9042`
* REDIS_TEST_SUPPORT_IMAGE: docker image name, default `redis`
